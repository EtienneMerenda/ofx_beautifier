# encoding: utf-8

"""Beautify OFX files to don't do that handmade ! (only tested on LBP France ofx file)

Put your .ofx file in same folder of this script and run it.
Beautify .ofx file wanna generate.

You can custom this file as you want ! =)

Next features: Add custom regex and types to customize this script.
"""

from ofxparse import OfxParser, OfxPrinter
from pprint import pprint
import re
import os

# Overwrite writeTrn function of ofxPrinter (remove upper type of transaction)


def writeTrn(self, trn, tabs=5):
    self.writeLine("<STMTTRN>", tabs=tabs)
    tabs += 1

    self.writeLine("<TRNTYPE>{}".format(trn.type), tabs=tabs)
    self.writeLine("<DTPOSTED>{}".format(
        self.printDate(trn.date)
    ), tabs=tabs)
    self.writeLine("<TRNAMT>{0:.2f}".format(float(trn.amount)), tabs=tabs)

    self.writeLine("<FITID>{}".format(trn.id), tabs=tabs)

    if len(str(trn.checknum)) > 0:
        self.writeLine("<CHECKNUM>{}".format(
            trn.checknum
        ), tabs=tabs)

    self.writeLine("<NAME>{}".format(trn.payee), tabs=tabs)

    if len(trn.memo.strip()) > 0:
        self.writeLine("<MEMO>{}".format(trn.memo), tabs=tabs)

    tabs -= 1
    self.writeLine("</STMTTRN>", tabs=tabs)

# Overwrite write function to add encoding rule


def write(self, filename=None, tabs=0):
    if filename is None:
        filename = self.out_filename

    with open(filename, 'w', encoding="utf-8") as f:
        self.writeToFile(f)


OfxPrinter.writeTrn = writeTrn
OfxPrinter.write = write

print('script launched')


class Patterns(object):

    class Title():

        def __init__(self):

            # group 1: Achat CB / group 2: name (keep only that) / group 3: date
            self.achat_cb = r'^(ACHAT CB) ([A-Z0-9\' \.\/-]+) ([0-9\.]+)$'

            # group 1: Virement / group 2: name (keep only that)
            self.virement = r'^(VIREMENT ?D?E?) ([A-Z0-9\' \.\/-]+)'

            # group 1: Prelevement / group 2: Identity
            self.prelevement = r'^(PRELEVEMENT DE) ([A-Z0-9 \.\/-]+)'

            # add apostrophe
            self.apostrophe = r'(?<= L) (?=[A-Z])'

    class Type():

        def __init__(self):

            self.POS = "Carte de crédit"
            self.DIRECTDEP = "Virement"
            self.PAYMENT = "Prélèvement"
            self.DEBIT = "Débit"
            self.ATM = "Retrait"
            self.CHECK = "Chèque"
            self.DEP = "Chèque"
            self.CREDIT = "Virement"
            self.INT = 'Cotisation & Intérets'


def label_beautify(label):

    patterns = Patterns.Title()

    if re.search(patterns.apostrophe, label):
        label = re.sub(patterns.apostrophe, "'", label)

    if re.search(patterns.achat_cb, label):
        return re.search(patterns.achat_cb, label).group(2).capitalize()

    elif re.search(patterns.virement, label):
        return re.search(patterns.virement, label).group(2).capitalize()

    elif re.search(patterns.prelevement, label):
        return re.search(patterns.prelevement, label).group(2).capitalize()

    else:
        return label.capitalize()


def type_beautify(type):

    types = Patterns.Type()

    return getattr(types, type.upper())


def beautify(transaction, i):

    if transaction.type == "DIRECTDEP":

        ofx.account.statement.transactions[i].payee = label_beautify(
            t.payee).title()

    else:

        ofx.account.statement.transactions[i].payee = label_beautify(
            t.payee).title()

    ofx.account.statement.transactions[i].type = type_beautify(t.type)


try:
    # Get all not beautified fox file in same folder
    ofx_files = [file_name for file_name in os.listdir(
    ) if '.ofx' in file_name and "beautified_" not in file_name]

    input_string = f'Choose your ofx file to beautify:\n' + \
        "\n".join([f"{number}: {ofx_name}" for number,
                   ofx_name in enumerate(ofx_files)]) + "\n > "

    choice = ofx_files[int(input(input_string))]

    with open(choice, 'r', encoding='utf-8') as f:
        ofx = OfxParser.parse(f)

        ofx_name = f.name

except FileNotFoundError as e:
    raise FileNotFoundError("Fichier introuvable !")

transactions = ofx.account.statement.transactions

# Beautify each transactions
for i, t in enumerate(transactions):
    beautify(t, i)

# Save work in new ofx
OfxPrinter(ofx, f'beautified_{ofx_name}').write()
